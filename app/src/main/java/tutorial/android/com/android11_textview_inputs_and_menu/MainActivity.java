
package tutorial.android.com.android11_textview_inputs_and_menu;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //registering for context menu
        registerForContextMenu(findViewById(R.id.editEmail));

        Button btn = (Button)findViewById(R.id.buttonAlert);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogFragment alertDialog = new AlertDialogFragment();
                alertDialog.show(getFragmentManager(), "alert dialog");
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.contextItem1:
                Toast.makeText(getApplicationContext(), "contextItem1 called.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.contextItem2:
                Toast.makeText(getApplicationContext(), "contextItem2 called.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //method is called every time right before the menu is shown
        Toast.makeText(getApplicationContext(), "onPrepareOptionsMenu called.", Toast.LENGTH_SHORT).show();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "action_settings menu item selected.", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(id == R.id.item1){
            Toast.makeText(getApplicationContext(), "item1 menu item selected.", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(id == R.id.item2){
            Toast.makeText(getApplicationContext(), "item2 menu item selected.", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(id == R.id.create_new){
            Toast.makeText(getApplicationContext(), "create_new menu item selected.", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(id == R.id.open){
            Toast.makeText(getApplicationContext(), "open menu item selected.", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
